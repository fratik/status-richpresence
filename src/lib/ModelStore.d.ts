import { Collection } from "discord.js";

type Model = {
  name: string,
  title: string,
  images: ModelImages,
  state: string
}

type ModelImages = {
  bigKey: string,
  bigText: string,
  smallKey: string,
  smallText: string
}

export class ModelStore extends Collection<string, Model> {
  constructor(models: Model[]);
}