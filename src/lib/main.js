/**
 * @typedef Config
 * @property {string} clientId ID clienta
 * @property {Model[]} models Modele Rich Presence
 */

/**
 * @typedef ModelImages
 * @property {string} bigKey Tytuł dużego zdjęcia
 * @property {string} smallKey Tytuł małego zdjęcia
 * @property {string} bigText Tekst dużego zdjęcia
 * @property {string} smallText Tekst małego zdjęcia
 */

/**
 * @typedef Model
 * @property {string} name Nazwa modelu
 * @property {string} title Tytuł modelu
 * @property {ModelImages} images Zdjęcia modelu
 * @property {string} state 'State' (podpis pod tytułem) modelu
 */
const rpc = require("discord-rpc");
const { ModelStore } = require("./ModelStore");

/**
 * Klasa błędów modeli
 */
class ModelError extends Error {

  /**
   * Konstruuje błąd
   * @param {string} message Wiadomość błędu
   * @param {Model} model Model, który spowodował błąd
   */
  constructor(message, model) {
    super(message);
    this.name = "ModelError";
    this.message = `Model ${model.name}: ${message}`;
  }
}
/**
 * Główna klasa
 */
class Main extends rpc.Client {

  /**
   * Konfiguruje klasę
   * @param {Config} config Konfiguracja
   */
  constructor(config) {
    super({ transport: "ipc" });
    this.destroyed = false;
    this.config = config;
    this.validateConfig();
    this.models = new ModelStore(config.models);
    super.login(config.clientId);
    this.activity = null;
  }

  /**
   * Sprawdza konfigurację
   * @private
   */
  validateConfig() {
    if (!this.config) throw new Error("Config nie istnieje");
    if (typeof this.config !== "object") throw new TypeError("Config musi być obiektem");
    if (!this.config.clientId) throw new Error("Brak ID clienta");
    if (typeof this.config.clientId !== "string") throw new TypeError("ID clienta musi być typu string");
    if (!this.config.models) throw new Error("Brak modeli");
    if ((this.config.models.constructor || {}).name !== "Array") throw new TypeError("Modele muszą być typu array");
    if (this.config.models.length === 0) throw new RangeError("Musisz ustawić co najmniej jeden model");
    for (const model of this.config.models) {
      if (!model.name || typeof model.name !== "string") throw new ModelError("Nazwa modelu musi być typu string", model);
      if (!model.title || typeof model.title !== "string") throw new ModelError("Tytuł modelu musi być typu string", model);
      if (model.images && typeof model.images !== "object") throw new ModelError("Zdjęcia modelu muszą być obiektem, lub niezdefiniowane", model);
      if (model.images) {
        if (model.images.bigKey && typeof model.images.bigKey !== "string") throw new ModelError("Zdjęcie 'big' (bigKey) musi być typu string, lub niezdefiniowane", model);
        if (model.images.smallKey && typeof model.images.smallKey !== "string") throw new ModelError("Zdjęcie 'small' (smallKey) musi być typu string, lub niezdefiniowane", model);
        if (model.images.bigText && typeof model.images.bigText !== "string") throw new ModelError("Text zdjęcia 'big' musi być typu string, lub niezdefiniowane", model);
        if (model.images.smallText && typeof model.images.smallText !== "string") throw new ModelError("Text zdjęcia 'small' musi być typu string, lub niezdefiniowane", model);
      }
      if (!model.state || typeof model.title !== "string") throw new ModelError("State (podpis POD 'title') modelu musi być typu string", model);
    }
  }

  /**
   * Zmienia/Ustawia model
   * @param {string} name Nazwa modelu
   * @param {Date} [startTimestamp=] Czas rozpoczęcia
   * @param {Date} [endTimestamp=] Czas zakończenia
   */
  changeModel(name, startTimestamp, endTimestamp) {
    if (this.destroyed) throw new Error("Klasa została wyłączona");
    if (!this.models.has(name)) throw new Error(`Nie znaleziono modelu ${name}`);
    const { title, images, state } = this.models.get(name);
    this.activity = {
      details: title,
      state,
      largeImageKey: images ? images.bigKey : undefined,
      largeImageText: images ? images.bigText : undefined,
      smallImageKey: images ? images.smallKey : undefined,
      smallImageText: images ? images.smallText : undefined,
      startTimestamp,
      endTimestamp
    };
    return super.setActivity(this.activity);
  }

  changeStartTimestamp(timestamp = new Date()) {
    if (timestamp.constructor.name !== "Date") throw TypeError("Timestamp musi być datą");
    this.activity["startTimestamp"] = timestamp;
    return super.setActivity(this.activity);
  }

  changeEndTimestamp(timestamp = new Date()) {
    if (timestamp.constructor.name !== "Date") throw TypeError("Timestamp musi być datą");
    this.activity["endTimestamp"] = timestamp;
    return super.setActivity(this.activity);
  }

  destroy() {
    this.destroyed = true;
    return super.destroy();
  }

}

module.exports = { Main };