const clearScreen = () => {
  process.stdout.write("\u001b[2J");
  process.stdout.write("\u001b[1;1H");
};
const inquirer = require("inquirer");
const { Main } = require("./lib/main");
const cfg = require("../config.json");
const main = new Main(cfg);
const delayer = (time) => { return new Promise(res => setTimeout(() => res(), time)); };
inquirer.registerPrompt("datetime", require("inquirer-datepicker-prompt"));
const zmianaModelu = async(firsttime = false) => {

  const answers = await inquirer.prompt([
    {
      type: "list",
      name: "rp",
      message: "Wybierz model Rich Presence",
      choices: cfg.models.map((m) => m.name)
    },
    {
      type: "list",
      name: "st",
      message: "Ustawić czas rozpoczęcia?",
      choices: ["Tak", "Nie"]
    },
    {
      type: "datetime",
      name: "stWhen",
      message: "Na kiedy mam ustawić?",
      when: ({st}) => st === "Tak",
      format: ["dd", "/", "mm", "/", "yyyy", " ", "HH", ":", "MM", ":", "ss"]
    },
    {
      type: "list",
      name: "et",
      message: "Ustawić czas zakończenia?",
      choices: ["Tak", "Nie"]
    },
    {
      type: "datetime",
      name: "etWhen",
      message: "Na kiedy mam ustawić?",
      when: ({et}) => et === "Tak",
      format: ["dd", "/", "mm", "/", "yyyy", " ", "HH", ":", "MM", ":", "ss"]
    }
  ]);
  await main.changeModel(answers.rp, answers.stWhen, answers.etWhen);
  clearScreen();
  console.log("Gotowe. Odczekaj 15 sekund..");
  await delayer(15e3);
  clearScreen();
  if (firsttime) return answers.rp;
  else menu(answers.rp);
};

const startTime = async(model) => {
  const { stWhen } = await inquirer.prompt({
    type: "datetime",
    name: "stWhen",
    message: "Na kiedy mam ustawić?",
    format: ["dd", "/", "mm", "/", "yyyy", " ", "HH", ":", "MM", ":", "ss"]
  });

  main.changeStartTimestamp(stWhen);
  return menu(model);
};

const endTime = async(model) => {
  const { etWhen } = await inquirer.prompt({
    type: "datetime",
    name: "etWhen",
    message: "Na kiedy mam ustawić?",
    format: ["dd", "/", "mm", "/", "yyyy", " ", "HH", ":", "MM", ":", "ss"]
  });

  main.changeEndTimestamp(etWhen);
  return menu(model);
};

const menu = async(model) => {
  clearScreen();
  console.log("Aktualny model to:", model);
  const { answer } = await inquirer.prompt([
    {
      type: "list",
      message: "Co chcesz zrobić?",
      name: "answer",
      choices: [
        {
          name: "Zmień model",
          value: "zm"
        },
        {
          name: "Zmień/Ustaw datę rozpoczęcia (\"stoper\" pod statusem)",
          value: "st"
        },
        {
          name: "Zmień/Ustaw datę zakończenia (\"odliczanie\" pod statusem)",
          value: "et"
        },
        new inquirer.Separator(),
        {
          name: "Zakończ program",
          value: "exit"
        }
      ]
    }
  ]);
  switch (answer) {
  case "zm": {
    clearScreen();
    return zmianaModelu();
  }
  case "st": {
    clearScreen();
    return startTime(model);
  }
  case "et": {
    clearScreen();
    return endTime(model);
  }
  case "exit": {
    clearScreen();
    await main.destroy();
    process.exit(0);
  }
  }

};

(async() => {
  clearScreen();
  const model = await zmianaModelu(true);
  await menu(model);
})();