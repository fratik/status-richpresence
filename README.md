# Status Rich Presence
Małe narzędzie do ustawiania własnych statusów na Discordzie.

### Konfiguracja
1. [Wejdź na tą stronę](https://discordapp.com/developers/applications/me), dodaj nową aplikację
2. Włącz Rich Presence w aplikacji
3. Dodaj zdjęcia
4. Zmień nazwę pliku `config.example.json` na `config.json`
5. Dopasuj go do swoich zastosowań
6. Gotowe!

### Uruchamianie
1. Pobierz [Node.js](https://nodejs.org/), [Yarn'a](https://yarnpkg.com/) i [git'a](https://git-scm.com)
2. Uruchom konsole/terminal w miejscu gdzie chcesz ściągnąć pliki i użyj `git clone git@gitlab.com:fratik/status-richpresence.git` (lub `git clone https://gitlab.com/fratik/status-richpresence.git`)
3. Użyj w konsoli `cd status-richpresence`
4. Użyj `yarn` 
5. Ostatecznie uruchom program za pomocą `node .`